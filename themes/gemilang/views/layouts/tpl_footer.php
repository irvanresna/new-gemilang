<footer>
                <div class="container">
                    <div class="bottom-footer">
				    <div class="col-md-8">
                        <p>
                        	<h4>Deliver your message and empower your brand through photo & video production</h4>
                        </p>
					</div>
						<div class="col-md-4">
                              <button class= "btn2">GET STARTED</button>
				        </div>
                    </div>
                    
                </div>
            </footer>
<?php

      $baseUrl = Yii::app()->theme->baseUrl; 

      $cs = Yii::app()->getClientScript();

      Yii::app()->clientScript->registerCoreScript('jquery');

    ?>
    
        <script src="<?php echo $baseUrl;?>/js/vendor/jquery-1.11.0.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/vendor/jquery.gmap3.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/plugins.js"></script>
        <script src="<?php echo $baseUrl;?>/js/main.js"></script>

    </body>
</html>