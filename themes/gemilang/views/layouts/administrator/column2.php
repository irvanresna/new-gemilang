<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/administrator/main'); ?>
<div class="caption">
        <h1 class="page-title"><?php print $this->pageTitle; ?></h1>
</div>
<div class="box-breadcumbs">
        <?php if(isset($this->breadcrumbs)):
                if ( Yii::app()->controller->route !== 'site/index' )
                    $this->breadcrumbs = array_merge(array (Yii::t('zii','<i class="icon-home"></i>')=>Yii::app()->homeUrl.'?r=site/index'), $this->breadcrumbs);
                    
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'homeLink'=>false,
                            'encodeLabel'=>false,
                            'tagName'=>'ul',
                            'separator'=>'',
                            'activeLinkTemplate'=>'<li><a href="{url}">{label}</a> <span class="divider">/</span></li>',
                            'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
                            'htmlOptions'=>array ('class'=>'breadcrumb')
                    ));
                    //<!-- breadcrumbs -->
        endif?>
</div>
<div class="row-fluid">
    <div id="content" class="span12">
            <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $message) {
                            echo '<div class="alert alert-' . $key . '">';
                            echo '<button type="button" class="close" data-dismiss="alert">�</button>';
                            print $message;
                            print "</div>\n";
                            
                            
                    }
            ?>
    
            <?php echo $content; ?>
            <hr>
            <?php
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title'=>'',
                ));
                $this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
                $this->endWidget();
            ?>
    </div>
</div>
<?php $this->endContent(); ?>
