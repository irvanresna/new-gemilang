<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
			<div id="about">
                <div class="about-img">
                      <img src="<?php echo $baseUrl;?>/images/about.jpg" alt="" />
                </div>
            </div>
			<div id="about-text">
				  <div class="col-md-6 txt1">
						<span>FLIQ EMPOWERS YOUR MARKETING CAMPAIGN BY DELIVERING SUCCESSFUL VISUAL MESSAGES THROUGH PHOTO AND VIDEO PRODUCTION</span>
				  </div>
				  <div class="col-md-6 txt2">
						<span>In this constantly evolving digital era that yields infinite supply of information and entertainment, the public attention is evidently obscured.
						Your message needs to be packaged carefully and effectively in order to grab the attention of modern consumers.
						What better way to achieve that than through visual imaging? A picture is indeed worth a thousand words.
						<br><br>
						GM is a full-service production company streamlining your marketing strategy with visual excellence targeted to impactfully reach your desired audience and consumer base.
						We work from strategy building, design and art direction, to delivering polished visuals that help your brand or business speak louder and better across all media platforms.
						<br><br>
						Our team consists of experienced creative minds, designers, developers, strategists, marketers and passionate field executors and post-production team put together with a mission not only to create stunning visual,
						but to optimize your campaign and fulfill your business objectives with it.</span>
				  </div>
			</div>
            <div id="services">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <center><h2>OUR CLIENTS</h2></center>
								<h4>Our one-stop visual shop caters to a clientele that spans over a wide range of industries. We work closely with our clients to design services that meet their image requirements across all media.</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/video-production.png" alt="" />
                                </div>
                                <h4>VIDEO PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/photo-production.png" alt="" />
                                </div>
                                <h4>PHOTO PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/web-development.png" alt="" />
                                </div>
                                <h4>WEB DEVELOPMENT & DESIGN</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/post-production.png" alt="" />
                                </div>
                                <h4>POST PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <div id="latest-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <center><h2>OUR PORTOFOLIO</h2></center
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio2.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">VIDEO PRODUCTION</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio1.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">PHOTO PRODUCTION</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio3.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">WEB DEVELOPMENT</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>