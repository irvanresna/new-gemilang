<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
<div id="top-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="home-account">
                                    <center><h3>WELCOME TO OUR WEBSITE</h3></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<div id="slider">
                <div class="flexslider">
                  <ul class="slides">
                    <li>
                      <img src="<?php echo $baseUrl;?>/images/home.jpg" alt="" />
                    </li>
                  </ul>
                </div>
            </div>

			<div class="col-md-12 tagline">
				  <center><h1>WE BLEND ART, TECHNOLOGY, AND STRATEGY TO CREATE EXTRAORDINARY VISUAL CONTENT</h1></center>
			
				  <center><h4>Headquartered in Jakarta, Indonesia, GM is a visual production company established to create powerful still and moving images to reinforce brands or businesses.
				  We are dedicated to craft visuals that are designed to communicate to the right people at the right time in exactly the right form.</h4></center>
			</div>
			
            <div id="services">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <center><h2>OUR SERVICE</h2></center>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/video-production.png" alt="" />
                                </div>
                                <h4>VIDEO PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/photo-production.png" alt="" />
                                </div>
                                <h4>PHOTO PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/web-development.png" alt="" />
                                </div>
                                <h4>WEB DEVELOPMENT & DESIGN</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <div class="icon">
                                    <img src="<?php echo $baseUrl;?>/images/post-production.png" alt="" />
                                </div>
                                <h4>POST PRODUCTION</h4>
                                <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu  sagittis vel diam in, malesuada malesuada risus. Aenean a sem leoneski.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <div id="latest-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <center><h2>OUR PORTOFOLIO</h2></center
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio2.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">VIDEO PRODUCTION</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio1.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">PHOTO PRODUCTION</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="blog-post">
                                <div class="blog-thumb">
                                    <img src="<?php echo $baseUrl;?>/images/portofolio3.jpg" alt="" />
                                </div>
                                <div class="blog-content">
                                    <div class="content-show">
                                        <h4><a href="single-post.html">WEB DEVELOPMENT</a></h4>
                                    </div>
                                    <div class="content-hide">
                                        <p>Sed egestas tincidunt mollis. Suspendisse rhoncus vitae enim et faucibus. Ut dignissim nec arcu nec hendrerit. Sed arcu odio, sagittis vel diam in, malesuada malesuada risus. Aenean a sem leo. Nam ultricies dolor et mi tempor, non pulvinar felis sollicitudin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>