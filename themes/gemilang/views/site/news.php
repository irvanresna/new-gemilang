<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
			<div id="about">
                <div class="about-img">
                      <img src="<?php echo $baseUrl;?>/images/news.jpg" alt="" />
                </div>
            </div>
            <div id="services" style="padding-bottom: 60px;">

            </div>
			<div id="news">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
							<h2>GM WEBSITE LAUNCH</h2>
							<h4>SEPTEMBER 01, 2015</h4>
						</div>
						<div class="col-md-6">
							<h4><span>In this constantly evolving digital era that yields infinite supply of information and entertainment, the public attention is evidently obscured.
						Your message needs to be packaged carefully and effectively in order to grab the attention of modern consumers.
						What better way to achieve that than through visual imaging? A picture is indeed worth a thousand words.
						<br><br>
						GM is a full-service production company streamlining your marketing strategy with visual excellence targeted to impactfully reach your desired audience and consumer base.
						We work from strategy building, design and art direction, to delivering polished visuals that help your brand or business speak louder and better across all media platforms.
						<br><br>
						Our team consists of experienced creative minds, designers, developers, strategists, marketers and passionate field executors and post-production team put together with a mission not only to create stunning visual,
						but to optimize your campaign and fulfill your business objectives with it.</span></h4>
						</div>
					</div>
				</div>
			</div>
			



