<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
			<div id="about">
                <div class="about-img">
                      <img src="<?php echo $baseUrl;?>/images/portofolio.jpg" alt="" />
                </div>
            </div>
			
			<div id="latest-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="contact-section">
                                <center><h2>CONTACT US</h2></center
                            </div>
                        </div>
						<div class="contact-section">
							  <div class="col-md-4">
								 <img src="<?php echo $baseUrl;?>/images/1.png" alt="" />
								 <span><h4>START YOUR PROJECT</h4></span>
								 <span class="konten">info@gemilangmultimedia.co.id</span>
							  </div>
							  <div class="col-md-4">
								  <img src="<?php echo $baseUrl;?>/images/career.png" alt="" />
								  <span><h4>CAREER</h4></span>
								 <span class="konten">JOIN OUR TEAM</span>
							  </div>
							  <div class="col-md-4">
								  <img src="<?php echo $baseUrl;?>/images/connect.png" alt="" />
								  <span><h4>CONNECT WITH US</h4></span>
								  <span class="sosmed"><img src="<?php echo $baseUrl;?>/images/twitter.png" alt="" /></span>
								  <span class="sosmed"><img src="<?php echo $baseUrl;?>/images/fb.png" alt="" /></span>
								  <span class="sosmed"><img src="<?php echo $baseUrl;?>/images/ig.png" alt="" /></span>
								  <span class="sosmed"><img src="<?php echo $baseUrl;?>/images/youtube.png" alt="" /></span>
							  </div>
							  <br><br><br>
							  <div class="col-md-4">
								 <img src="<?php echo $baseUrl;?>/images/loc.png" alt="" />
								 <span><h4>Jl. Pertengahan no.29 Jakarta 13770</h4></span>
							  </div>
							  <div class="col-md-4">
								  <img src="<?php echo $baseUrl;?>/images/call.png" alt="" />
								  <span><h4>+62 21 71203740</h4></span>
							  </div>
							  <div class="col-md-4">
								  <img src="<?php echo $baseUrl;?>/images/bb.png" alt="" />
								  <span><h4>-</h4></span>
							  </div>
						</div>
                    </div>
                </div>
            </div>